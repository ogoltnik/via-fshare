# Semestrální práce z předmětu VIA

autor: Nikolai Ogoltsov (ogoltnik)

### Popis

FShare je webová aplikace pro sdílení souborů, která umožňuje současnou práci s privátním (Resource Service) a veřejným (Google Drive) souborovým systémem. Autorizovaný uživatel (může se přihlásit pomoci účtu Google anebo pomoci interního účtu) má možnost nejenom nahrávat/mazat soubory ale i přenášet je z jednoho uložiště do druhého. 

### Architektura

![](achitecture%20diagram.png)

### Technologie

#### WEB App 

- Angular 8
- RxJS
- Angular Material

#### Gateway API

* Spring Boot 2
* Hystrix
* Feign
* RxJava 2

#### Resource Service

* Spring Boot 2
* Spring Data
* Swagger UI
* Spring WebFlux ?
* Tomcat / Netty

#### Database

- Postgres 11

#### Auth

- Oauth 2.0

#### Reverse Proxy

- Nginx / Traefik